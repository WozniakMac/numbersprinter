# frozen_string_literal: true

require_relative 'divisible_numbers_replacer'
require_relative 'separate_lines_array_printer'

class NumbersPrinter
  def initialize(range:, preprocessor:, printing_engine:)
    @range = range
    @preprocessor = preprocessor
    @printing_engine = printing_engine
  end

  def run
    preprocessed_numbers = @preprocessor.process(@range)
    @printing_engine.print(preprocessed_numbers)
  end
end

if __FILE__ == $0
  printer = NumbersPrinter.new(
    range: (1..100),
    preprocessor: DivisibleNumbersReplacer.new,
    printing_engine: SeparateLinesArrayPrinter.new
  )

  printer.run
end
