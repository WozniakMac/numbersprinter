# frozen_string_literal: true

class SeparateLinesArrayPrinter
  def print(array)
    array.each do |element|
      puts element
    end
  end
end
