# frozen_string_literal: true

class DivisibleNumbersReplacer
  def process(numbers_range)
    numbers_range.map do |number|
      if divisible_by_3?(number) && divisible_by_5?(number)
        'People Power'
      elsif divisible_by_3?(number)
        'People'
      elsif divisible_by_5?(number)
        'Power'
      else
        number
      end
    end
  end

  private

  def divisible_by_3?(number)
    number % 3 == 0
  end

  def divisible_by_5?(number)
    number % 5 == 0
  end
end
